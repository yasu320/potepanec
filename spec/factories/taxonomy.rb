FactoryBot.define do
  factory :taxonomy, class: 'Spree::Taxonomy' do
    id                { 1 }
    name              { "taxonomy" }
  end
end
