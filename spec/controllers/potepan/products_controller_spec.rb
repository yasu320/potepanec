RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    context "as a visitor" do
      let(:product) { create(:product) }

      it "responds successfully" do
        get :show, params: { id: product.id }
        expect(response).to be_successful
      end
      it "returns a 200 response" do
        get :show, params: { id: product.id }
        expect(response).to have_http_status "200"
      end
      it "assigns @product" do
        get :show, params: { id: product.id }
        expect(assigns(:product)).to eq product
      end
      it "renders the show template" do
        get :show, params: { id: product.id }
        expect(response).to render_template(:show)
      end
    end
  end
end
